﻿using IronPython.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appraiser.Services
{
    public class PythonService : IPythonService
    {

        private readonly IConfiguration _config;

        public PythonService(IConfiguration config)
        {
            _config = config;
        }
        public bool RunPythonScript(int appId)
        {
            // create process info
            var psi = new ProcessStartInfo();
            var pythonPath = _config.GetValue<string>("AppSettings:PythonPath");    
            var scriptName = _config.GetValue<string>("AppSettings:PythonScriptName");

            psi.FileName = pythonPath; 

            // provide script and args
            var scriptPath = Path.GetFullPath(Directory.GetCurrentDirectory() + @"\Scripts\" + scriptName);

            // todo: define args 

            psi.Arguments = $"\"{scriptPath}\" \"{appId}\"";

            
            //process configuration

            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;

            // execute process and get output

            var errors = "";
            var results = "";
            try
            {
                using (var process = Process.Start(psi))
                {
                    errors = process.StandardError.ReadToEnd();
                    results = process.StandardOutput.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                return false;
            }           

            return true;
        }
    }
}
