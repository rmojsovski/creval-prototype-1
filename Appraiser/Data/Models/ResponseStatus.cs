﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Appraiser.Data.Models
{
    public enum ResponseStatus
    {
        Pending = 0,
        Success = 1,
        Error = 2,
        NotFound = 3,
    }
}
