﻿using Appraiser.Data.Entities;
using Microsoft.EntityFrameworkCore;
using MySql.EntityFrameworkCore.Extensions;

namespace Appraiser.Data.Entities
{
    public class AppraiserContext: DbContext
    {
        public AppraiserContext(DbContextOptions<AppraiserContext> options):base(options)
        {

        }
        public DbSet<AppraisalRequest> AppRequests { get; set; }
        public DbSet<AppraisalResponse> AppResponses { get; set; }
        public DbSet<PropertyData> PropertyData { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           // optionsBuilder.UseMySQL("server=localhost;database=library;user=user;password=password");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {                      
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<PropertyData>().Property(prop => prop.latitude).HasPrecision(11, 8);
            modelBuilder.Entity<PropertyData>().Property(prop => prop.longitude).HasPrecision(11, 8);

            //modelBuilder.Entity<AppraisalRequest>(entity =>
            //{
            //    entity.HasKey(e => e.Id);
            //    entity.Property(e => e.PropertyType).IsRequired();
            //    entity.Property(e => e.NumUnits);
            //});           
        }
    }
}
