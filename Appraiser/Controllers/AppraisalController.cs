﻿using Appraiser.Data;
using Appraiser.Data.Models;
using Appraiser.Data.Models.Request;
using Appraiser.Data.Models.Response;
using Appraiser.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Appraiser.Controllers
{
    [Authorize]
    [ApiController]  
    public class AppraisalController : Controller
    {
        private IAppraiserService _appraiserService;
        private IPythonService _pythonService;
        
        public AppraisalController(IAppraiserService appraiserService, IPythonService pythonService)
        {
            _appraiserService = appraiserService;
            _pythonService = pythonService;
        }

        // POST: AppraisalController/requestAppraisal
        [HttpPost]
        [Route("api/requestappraisal")]
        [ProducesResponseType(typeof(AppraisalResponseDTO), StatusCodes.Status200OK)]              
        public async Task<ActionResult> requestAppraisal(AppraisalRequestDTO request)
        {
            if (ModelState.IsValid)
            {
                // 1. create appraiser request 
                var appId = await _appraiserService.RequestAppraisal(request);

                if (appId != -1)
                {                    

                    // 2. Run Python script for the created request
                    _pythonService.RunPythonScript(appId);

                    // 3. Read the response and send it back
                    var response = await _appraiserService.GetResponse(appId);
                                        

                    //  4return result
                    return Ok(response);
                }
                else
                {
                    return Ok(new AppraisalResponseDTO() { StatusCode = (int)ResponseStatus.Error, Status = "An Error occured!" });
                }               
                
            }
                
            else
            {
                return BadRequest(ModelState);
            }
               
        }
        
    }
}
