﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using MySql.EntityFrameworkCore.Metadata;

namespace Appraiser.Migrations
{
    public partial class initmodeldedicatedserver : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppRequests",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ValuationId = table.Column<int>(type: "int", nullable: false),
                    EmailId = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    PropType = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false),
                    ParcelNumber = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    AdjPropTypes = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: false),
                    NumParkingOpen = table.Column<int>(type: "int", nullable: false),
                    NumParkingCovered = table.Column<int>(type: "int", nullable: false),
                    NumParkingGarage = table.Column<int>(type: "int", nullable: false),
                    NumParkingSecureGarage = table.Column<int>(type: "int", nullable: false),
                    NumUnits = table.Column<int>(type: "int", nullable: false),
                    PropSubCategory = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false),
                    PropSubCategoryOther = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    TotalParkingSpaces = table.Column<int>(type: "int", nullable: false),
                    YearBuilt = table.Column<int>(type: "int", nullable: false),
                    RemodelAddition = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: false),
                    RemodelAdditionYearRange = table.Column<string>(type: "varchar(15)", maxLength: 15, nullable: true),
                    OriginalPurchasePrice = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    OriginalFinancingAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    PropStrName = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false),
                    PropStrNumber = table.Column<int>(type: "int", nullable: false),
                    PropCounty = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false),
                    PropCity = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false),
                    PropState = table.Column<string>(type: "varchar(2)", maxLength: 2, nullable: false),
                    PropZipCode = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: false),
                    CurrentMonthRental = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    RentLow = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: false),
                    FamilyCharityRent = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: false),
                    FamilyCharityRentCmnts = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    PropAdditionalAddress = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: false),
                    VacRateCmnts = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: false),
                    RepairRemodel = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: false),
                    RepairRemodelCmnts = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    PropAdditionalInfo = table.Column<string>(type: "varchar(250)", maxLength: 250, nullable: true),
                    NumUnitsStudioConv = table.Column<int>(type: "int", nullable: false),
                    NumUnits1Br = table.Column<int>(type: "int", nullable: false),
                    NumUnits2Br = table.Column<int>(type: "int", nullable: false),
                    NumUnits3BrOrMore = table.Column<int>(type: "int", nullable: false),
                    BuildingSqFt = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    NumFloors = table.Column<int>(type: "int", nullable: false),
                    NumBedrooms = table.Column<int>(type: "int", nullable: false),
                    SaleDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Latitude = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    Longitude = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    NetOperatingIncome = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: false),
                    StatusCode = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppRequests", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PropertyData",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    propertyAddress = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    propertyType = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    saleCond = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    dateSale = table.Column<DateTime>(type: "datetime", nullable: false),
                    priceSale = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    sfLand = table.Column<int>(type: "int", nullable: false),
                    sfBldg = table.Column<int>(type: "int", nullable: false),
                    yrBlt = table.Column<int>(type: "int", nullable: false),
                    parkingSp = table.Column<int>(type: "int", nullable: false),
                    units = table.Column<int>(type: "int", nullable: false),
                    bldgClass = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    starRating = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    grossRentMultiplier = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    noi = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    expTot = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    cap = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    cond = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    zeroBr = table.Column<int>(type: "int", nullable: false),
                    oneBr = table.Column<int>(type: "int", nullable: false),
                    twoBr = table.Column<int>(type: "int", nullable: false),
                    threeBr = table.Column<int>(type: "int", nullable: false),
                    levels = table.Column<int>(type: "int", nullable: false),
                    incGross = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    priceasking = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    downPayment = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    latitude = table.Column<decimal>(type: "decimal(18, 2)", precision: 11, scale: 8, nullable: false),
                    longitude = table.Column<decimal>(type: "decimal(18, 2)", precision: 11, scale: 8, nullable: false),
                    salePriceComment = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyData", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AppResponses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    AppraisalRequestId = table.Column<int>(type: "int", nullable: false),
                    ValuationId = table.Column<int>(type: "int", nullable: false),
                    ValuationAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    ReliabilityScore = table.Column<int>(type: "int", nullable: false),
                    RecommendationText = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    CredConfInterval = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    ValuationDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsLevel1Sufficient = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: false),
                    StatusCode = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppResponses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppResponses_AppRequests_AppraisalRequestId",
                        column: x => x.AppraisalRequestId,
                        principalTable: "AppRequests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppResponses_AppraisalRequestId",
                table: "AppResponses",
                column: "AppraisalRequestId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppResponses");

            migrationBuilder.DropTable(
                name: "PropertyData");

            migrationBuilder.DropTable(
                name: "AppRequests");
        }
    }
}
