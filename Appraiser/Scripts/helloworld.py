# -*- coding: utf8 -*-
"""
Capture Risk Evaluation Level 1 Rule-based Approach
=====================================

Implements the level 1 goals as defined by "CR Level 1 goals.docx".
The parameters defined in `II. Data collection [Data selector algorithm]` and `III. Prediction` are defined as a python dictionary in :func:`run`
"""

import datetime as dt
import sys

import geopy.distance
import mysql.connector as connection
import numpy as np
import pandas as pd


def query_sql_to_pandas(con_data, query):
    """
    Sends a query to an SQL server and returns the result as a pandas dataframe.
    Note that the query must result in rows returned, `INSERT` is thus not valid.
    To use an `INSERT` query see :func:`query_insert_rows`

    :param con_data: connection data of the format host, database, user, password, port
    :param query: SQL query to send
    :return: pandas dataframe containing the result of the query
    """
    try:
        mydb = connection.connect(
            host=con_data[0],
            database=con_data[1],
            user=con_data[2],
            passwd=con_data[3],
            port=con_data[4]
        )

        result_dataFrame = pd.read_sql(query, mydb)
        mydb.close()
        return result_dataFrame
    except Exception as e:
        mydb.close()
        print(str(e))

    return None


def query_insert_rows(con_data, query):
    """
    Executes a given query, such as `INSERT` on an SQL server.
    Note that no value is returned in python.
    To get data out of an SQL database see :func:`query_sql_to_pandas`

    :param con_data: connection data of the format host, database, user, password, port
    :param query: SQL query to send
    """
    mydb = connection.connect(
        host=con_data[0],
        database=con_data[1],
        user=con_data[2],
        passwd=con_data[3],
        port=con_data[4]
    )

    cursor = mydb.cursor()

    # Insert new row
    cursor.execute(
        query)

    # Make sure data is committed to the database
    mydb.commit()

    cursor.close()
    mydb.close()


def get_input(con_data, app_id):
    """
    Given connection data to an SQL server and a valid `ID` of a row, fetches and prepares the input for the model.

    :param con_data: connection data of the format host, database, user, password, port
    :param app_id: ID of the input row to be fetched
    :return: pandas dataframe containing the input as a single row
    """
    df = query_sql_to_pandas(con_data=con_data, query=f"SELECT * FROM appraiser.apprequests WHERE Id = {app_id};")

    df['totalBr'] = df.NumBedrooms
    df['units'] = df.NumUnits
    df['parkingSp'] = df.TotalParkingSpaces
    df['parkingSp_per_Br'] = np.where(df['totalBr'] == 0, np.nan, df.parkingSp / df['totalBr'])
    # df['SaleDate'] = '2022-02-24'

    # update Riste
    df['EstOpIncome'] = df['EstOpIncome'].astype(float)

    return df


def get_dataset(con_data):
    """
    Fetches and prepares the dataset from an SQL server.

    :param con_data: connection data of the format host, database, user, password, port
    :return: dataset as a pandas dataframe
    """
    df_risk = query_sql_to_pandas(con_data=con_data, query="SELECT * FROM appraiser.propertydata;")

    # Historical data
    df_risk = df_risk[~df_risk.propertyAddress.isna()]
    # df_risk = df_risk[df_risk.saleCond == 'CashEquiv'] # Not needed here - For next Levels

    df_risk['GIM'] = np.where(df_risk['incGross'] == 0, np.nan, df_risk['priceSale'] / df_risk['incGross'])
    df_risk['totalBr'] = df_risk.oneBr * 1 + df_risk.twoBr * 2 + df_risk.threeBr * 3
    df_risk['parkingSp_per_Br'] = np.where(df_risk['totalBr'] == 0, 1, df_risk.parkingSp / df_risk['totalBr'])

    return df_risk


def apply_model(df_risk, df, params, app_id):
    """
    Performs the actual model scoring, based on a dataset, input and parameters and inserts the result into the SQL output table.
    The app_id is necessary for the final insert query.

    :param df_risk: dataset which to use for the model (nearest neighbours)
    :param df: input row
    :param params: parameters, defining the level 1 constraints
    :param app_id: ID of the input
    """

    # def compute_distance(row, coords_1):
    #   coords_2 = (row.latitude, row.longitude)
    #   return geopy.distance.distance(coords_1, coords_2).miles

    con_data = ["92.205.62.170", "appraiser", "appraiser", "@ppr@15er", "3306"]
    time_span = params["time_span"]
    n_units = params["n_units"]
    year_built = params["year_built"]
    gross_liv_area = params["gross_liv_area"]
    tot_bedr = params["tot_bedr"]
    park_per_bedr = params["park_per_bedr"]
    levels = params["levels"]
    radius = params["radius"]
    price_per_unit_weight = params["price_per_unit_weight"]
    price_per_gim_weight = params["price_per_gim_weight"]
    price_per_bedr_weight = params["price_per_bedr_weight"]
    cap_rate_weight = params["cap_rate_weight"]

    # Radius calculation
    coords_1 = (df.Latitude.iloc[0], df.Longitude.iloc[0])
    # df_risk['radius'].apply(lambda x : compute_distance(x, coords_1), axis=1)

    df_risk['radius'] = None

    for i in range(len(df_risk)):
        coords_2 = (df_risk.latitude.iloc[i], df_risk.longitude.iloc[i])
        df_risk['radius'].values[i] = geopy.distance.distance(coords_1, coords_2).miles

    # Conditions
    # Property type match
    df_comps = df_risk[(  # (df_risk.propertyType == df.propertyType.iloc[0]) & # Not needed here - for next levels

        # Radius:
            (df_risk.radius <= radius) &
            # Time span is 5 years back
            (df_risk.dateSale >= df.SaleDate.iloc[0] - pd.offsets.DateOffset(years=time_span)) & (
                    df_risk.dateSale < df.SaleDate.iloc[0]) &
            # Number of units:
            (df_risk.units >= df.units.iloc[0] * (1 - n_units[0])) & (
                    df_risk.units <= df.units.iloc[0] * (1 + n_units[1])) &
            # Year built:
            (df_risk.yrBlt >= df.YearBuilt.iloc[0] - year_built[0]) & (
                    df_risk.yrBlt <= df.YearBuilt.iloc[0] + year_built[1]) &
            # Gross living area:
            (df_risk.sfBldg >= df.BuildingSqFt.iloc[0] * (1 - gross_liv_area[0])) & (
                    df_risk.sfBldg < df.BuildingSqFt.iloc[0] * (1 + gross_liv_area[0])) &
            # Total bedrooms:
            (df_risk.totalBr >= df.totalBr.iloc[0] * (1 - tot_bedr[0])) & (
                    df_risk.totalBr < df.totalBr.iloc[0] * (1 + tot_bedr[1])) &
            # Parking per bedroom ratio:
            (df_risk.parkingSp_per_Br >= df.parkingSp_per_Br.iloc[0] * (1 - park_per_bedr[0])) & (
                    df_risk.parkingSp_per_Br < df.parkingSp_per_Br.iloc[0] * (1 + park_per_bedr[0])) &

            # Levels
            (((df_risk.levels == levels[0][0]) & (df.NumFloors.iloc[0] == levels[0][1])) |
             ((df_risk.levels == levels[1][0]) & (df.NumFloors.iloc[0] == levels[1][1])) |
             ((df_risk.levels >= levels[2][0][0]) & (df_risk.levels <= levels[2][0][1]) & (
                     df.NumFloors.iloc[0] == levels[2][1])) |
             ((df_risk.levels >= levels[3][0][0]) & (df_risk.levels <= levels[3][0][1]) & (
                     df.NumFloors.iloc[0] == levels[3][1])) |
             ((df_risk.levels >= levels[4][0][0]) & (df_risk.levels <= levels[4][0][1]) & (
                     df.NumFloors.iloc[0] == levels[4][1])) |
             ((df_risk.levels >= levels[5][0][0]) & (df_risk.levels <= levels[5][0][1]) & (
                     df.NumFloors.iloc[0] >= levels[5][1][0]) & (df.NumFloors.iloc[0] <= levels[5][1][1])) |
             ((df_risk.levels >= df.NumFloors.iloc[0] * (1 - levels[6][0][0])) & (
                     df_risk.levels <= df.NumFloors.iloc[0] * (1 + levels[6][0][1])) & (
                      df.NumFloors.iloc[0] >= levels[6][1]))

             ))]

    # Reliability score
    if len(df_comps) <= 3:
        reliability_score = 1
        recommended_level = 'Appraisal full'
    elif len(df_comps) <= 5:
        reliability_score = 2
        recommended_level = 'Appraisal full'
    elif len(df_comps) <= 8:
        reliability_score = 3
        recommended_level = 'Appraisal limited'
    elif len(df_comps) <= 12:
        reliability_score = 4
        recommended_level = 'Appraisal or evaluation'
    elif len(df_comps) <= 20:
        reliability_score = 5
        recommended_level = 'Evaluation or Level I'
    elif len(df_comps) <= 40:
        reliability_score = 6
        recommended_level = 'CR Level I return value'
    else:
        reliability_score = 7
        recommended_level = 'CR Level I return value'

    # Predictions
    df_comps_units = np.where( (df_comps['units'].isna()) | (df_comps['units'] == 0 ), np.nanmedian(df_comps['units']), df_comps['units'] )
    df_comps_priceSale = np.where( (df_comps['priceSale'].isna() ) | (df_comps['priceSale'] == 0 ), 0, df_comps['priceSale'] )
    df_units_isNULL = np.where(((df['units'].isna()) | (df['units'] == 0)), 1, df['units'])
    price_per_units = np.nanmedian(df_comps_priceSale / df_comps_units)
    price_per_units_median_weight = (price_per_units * df_units_isNULL) * price_per_unit_weight

    # Anual Income:  incGross = df['CurrentMonthRental'] * 12
    df_comps_GIM = np.where(np.nanmedian(df_comps['GIM']) == np.nan, 1, np.nanmedian(df_comps['GIM']))
    df_CurrMonthRental = np.where(df['CurrentMonthRental'].isnull(), 1, df['CurrentMonthRental'])
    price_per_GIM_median_weight = (df_comps_GIM * df_CurrMonthRental * 12) * price_per_gim_weight

    # Bedroom price
    df_comps_bedrooms = np.where( (df_comps['totalBr'].isna()) | (df_comps['totalBr'] == 0), np.nanmedian(df_comps['totalBr']), df_comps['totalBr'] )
    df_bedrooms_isNULL = np.where( ( (df['totalBr'].isna()) | (df['totalBr'] == 0 ) ), 1, df['totalBr'] )
    price_per_bedroom = np.nanmedian(df_comps_priceSale / df_comps_bedrooms)
    price_per_bed_median_weight = (price_per_bedroom * df_bedrooms_isNULL) * price_per_bedr_weight

    # Net Operating Income: noi = df['NetOperatingIncome'] * df['CurrentMonthRental'] * 12
    df_noi = np.where(df['EstOpIncome'].isnull(), 1, df['EstOpIncome'])
    #df_cmr = np.where(df['CurrentMonthRental'].isnull(), 1, df['CurrentMonthRental'])
    df_comps_cap = np.where( ((np.nanmedian(df_comps['cap']) == 0) | (np.nanmedian(df_comps['cap']) == np.nan)), 1, np.nanmedian(df_comps['cap']))
    cap_rate_median_weight = ( df_noi * 100 / df_comps_cap) * cap_rate_weight
    prediction_total = np.nansum(
        [price_per_units_median_weight, price_per_GIM_median_weight, 
        price_per_bed_median_weight, cap_rate_median_weight])

    loan_amount = df['LoanAmtRequest']
    ltv_ratio = loan_amount / np.where(prediction_total <= 0, 1, prediction_total)
    if reliability_score >= 5 and ltv_ratio[0] < params['ratio_sufficient']:
        level_1 = 'YES' #is sufficient
    else:
        level_1 = 'NO' #is insufficient
    """
    if prediction_total <= 0:
        prediction_total = np.nan
    else:
        prediction_total = prediction_total
    """

    # update Riste
    status_code = 1  ## 0: pending; 1: success; 2: error
    status_text = 'Success'
    # print(prediction_total)
    #print(price_per_units_median_weight)
    #print(df_comps_cap)
    #print(df_noi)
    #print(cap_rate_median_weight)

    # update Riste
    query = f"UPDATE appraiser.appresponses SET  ValuationAmount={prediction_total}, reliabilityScore={reliability_score}, recommendationText='{recommended_level}', StatusCode={status_code}, Status='{status_text}', IsLevel1Sufficient='{level_1}' WHERE AppraisalRequestId = {app_id};"
    # print("query: ", query)
    # Insert the prediction result into the Output table
    query_insert_rows(con_data, query)


def run(app_id):
    """
    Executes the model scoring pipeline given an ID.

    :param app_id: ID which is to be fetched and scored
    """
    con_data = ["92.205.62.170", "appraiser", "appraiser", "@ppr@15er", "3306"]
    df_input = get_input(con_data, app_id)
    df_property_data = get_dataset(con_data)

    # parameters
    params = {
        "time_span": 5,  # For how much time back from Sales' date to search
        "n_units": [0.3, 0.4],  # Number of units:  x% down, y% up from subject number of units
        "year_built": [20, 20],  # Year built: x years down, y years up from subject year built
        "gross_liv_area": [0.5, 1.0],  # Gross living area:  x% down, y% up from subject gross living area
        "tot_bedr": [0.5, 1.0],  # Total bedrooms (calculated field):  x% down, y% up from subject total bedrooms
        "park_per_bedr": [0.3, 0.4],  # Parking per bedroom ratio (calculated field):  x% down, y% up from subject ratio
        "levels": [[1, 1], [2, 2], [[2, 4], 3], [[3, 5], 4], [[4, 6], 5], [[5, 12], [6, 10]], [[0.5, 0.7], 11]],
        # Meet the criteria if match between subject and search levels criterias
        "radius": 2,  # Max Distance (in miles) to search from subject location (based on latitude and longitude)
        "ratio_sufficient": 0.6, #Treashold for if Level 1 is sufficient or not
        "price_per_unit_weight": 0.35,  # From 0 to 1
        "price_per_gim_weight": 0.25,  # From 0 to 1
        "price_per_bedr_weight": 0.2,  # From 0 to 1
        "cap_rate_weight": 0.2,  # From 0 to 1
    }

    # TODO: def
    assert np.isclose(
        params["price_per_unit_weight"] + params["price_per_gim_weight"] + params["price_per_bedr_weight"] + params[
            "cap_rate_weight"], 1)

    # Apply the rules given the parameters
    apply_model(df_property_data, df_input, params, app_id)


def main():
    """
    Entry point in case the level 1 script is run as-is. Simply calls :func:'run' given an ID.

    """
    reqid = 84
    run(reqid)

    
if __name__ == "__main__": 
    reqid=int(sys.argv[1])
    run(reqid)
    sys.exit(0)
